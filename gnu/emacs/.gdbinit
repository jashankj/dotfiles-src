set $INTPTR_MAX		= 0x7fffffffffffffff

set $GCTYPEBITS		= 3
set $EMACS_INT_MAX	= 0x7fffffffffffffff
set $INTTYPEBITS	= ($GCTYPEBITS - 1)
set $VAL_MAX		= ($EMACS_INT_MAX >> ($GCTYPEBITS - 1))
set $USE_LSB_TAG	= ($VAL_MAX / 2 < $INTPTR_MAX)
set $VALMASK		= ($USE_LSB_TAG ? - (1 << $GCTYPEBITS) : $VAL_MAX)
set $GCALIGNMENT	= 8
